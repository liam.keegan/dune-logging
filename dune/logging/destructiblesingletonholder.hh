// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef DUNE_LOGGING_DESTRUCTIBLESINGLETONHOLDER_HH
#define DUNE_LOGGING_DESTRUCTIBLESINGLETONHOLDER_HH

#include <cassert>
#include <memory>

#include <dune/logging/exceptions.hh>

namespace Dune::Logging {

#ifndef DOXYGEN

  // forward declaration
  template<typename Singleton, typename Factory>
  class DestructibleSingletonHolder;

#endif

  /**
   * \addtogroup Utilities
   * \{
   */

  //! Returns the DestructibleSingletonHolder for the given singleton and factory.
  /**
   * \sa DestructibleSingletonHolder
   */
  template<typename Singleton, typename Factory>
  DestructibleSingletonHolder<Singleton,Factory>& destructibleSingleton(Factory factory)
  {
    static DestructibleSingletonHolder<Singleton,Factory> holder(std::move(factory));
    return holder;
  }

  //! A holder for a singleton that can be created and destroyed multiple times.
  /*
   * This class manages an internal instance of the given class `Singleton`. It is possible to test whether
   * the object has been created, to explicitly create the object with the passed-in factory and to destroy
   * the singleton, after wich it can be created once more.
   *
   * `Factory` is an invocable object that will be called with the arguments passed to create. It must
   * return a `std::unique_ptr<Singleton>`.
   *
   * Typically, the factory will be a private member function of the actual singleton object, which then also
   * contains a member function for accessing the singleton. The second member function can then pass the factory
   * function to the `DestructibleSingletonHolder`.
   *
   */
  template<typename Singleton, typename Factory>
  class DestructibleSingletonHolder
  {

    friend DestructibleSingletonHolder& destructibleSingleton<Singleton,Factory>(Factory factory);

    std::unique_ptr<Singleton> _data;
    Factory _factory;

    // private constructor, can only be called by destructibleSingleton()
    DestructibleSingletonHolder(Factory factory)
      : _factory(std::move(factory))
    {}

  public:

    //! Creates singleton by calling the stored factory with the passed-in arguments.
    template<typename... T>
    void create(T&&... args)
    {
      if (_data)
        DUNE_THROW(SingletonError,"Singleton already created");
      _data = _factory(std::forward<T>(args)...);
    }

    //! Returns whether the contained object is currently valid (created and not destroyed).
    operator bool() const
    {
      return bool(_data);
    }

    //! Returns a reference to the stored singleton.
    Singleton& get()
    {
      assert(_data);
      return *_data;
    }

    //! Destroys the contained singleton object.
    void destroy()
    {
      if (not _data)
        DUNE_THROW(SingletonError,"Singleton not present");
      _data.reset();
    }

  };

  /**
   * \}
   */

} // Dune::Logging


#endif // DUNE_LOGGING_DESTRUCTIBLESINGLETONHOLDER_HH
