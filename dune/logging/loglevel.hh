// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef DUNE_LOGGING_LOGLEVEL_HH
#define DUNE_LOGGING_LOGLEVEL_HH

#include <chrono>
#include <ctime>
#include <optional>
#include <string_view>

#include <dune/logging/fmt.hh>

namespace Dune::Logging {

  /**
   * \addtogroup logging
   * \{
   */

  //! Severity level for log messages.
  enum class LogLevel
  {

    //! Designates that log messages are disabled.
    /**
     * \note This flag is only intended for setting the log level of a logging component, **never**
     *       use this level when logging a message!
     */
    off = 0,

    //! A critical error during program execution, forcing the program to abort.
    critical = 3,

    //! A recoverable error, from which the program might recover and continue.
    error = 6,

    //! A warning about some problem, but which does not necessarily require an immediate reaction.
    warning = 9,

    //! Information about the high-level program flow.
    notice = 12,

    //! Somewhat finer information about the program flow, still interesting in most cases.
    info = 15,

    //! Detailed information about the program flow, like per-iteration information in solvers.
    detail = 18,

    //! Debug-level information that is normally only interesting when debugging a program.
    debug = 21,

    //! Fine-grained debug information that should not be turned on for the whole program.
    /**
     * Log messages at this level could for example be used to trace the entry into and exit out of
     * a given function.
     */
    trace = 24,

    //! Designates that all log messages are enabled.
    /**
     * \note This flag is only intended for setting the log level of a logging component, **never**
     *       use this level when logging a message!
     */
    all = 30

  };

  //! Returns the name of a given log level.
  /**
   * \note This function will throw an exception of type LoggingError if level is not a valid
   *       LogLevel.
   */
  std::string_view name(LogLevel level);

  //! Returns the name of a given log level, padded to the widest width of a log level name.
  /**
   * \note This function will throw an exception of type LoggingError if level is not a valid
   *       LogLevel.
   */
  std::string_view paddedName(LogLevel level);

  //! Parses the given string into the corresponding LogLevel.
  /**
   * \note This function will throw an exception of type LoggingError if name is not the name of a
   *       valid LogLevel.
   */
  LogLevel parseLogLevel(std::string_view name);

} // end namespace Dune::Logging


#ifndef DOXYGEN

namespace fmt {

  // make it possible to format a LogLevel in a log message

  template <typename Char>
  struct formatter<Dune::Logging::LogLevel,Char>
    : public formatter<fmt::basic_string_view<Char>,Char>
  {

    using Base = formatter<fmt::basic_string_view<Char>,Char>;

    // we just inherit the parsing from the original formatter

    template <typename FormatContext>
    auto format(Dune::Logging::LogLevel level, FormatContext& ctx) {
      return Base::format(name(level),ctx);
    }

  };

} // namespace fmt

#endif // DOXYGEN

#endif // DUNE_LOGGING_LOGLEVEL_HH
