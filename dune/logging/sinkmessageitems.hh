// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef DUNE_LOGGING_SINKMESSAGEITEMS_HH
#define DUNE_LOGGING_SINKMESSAGEITEMS_HH

#include <cassert>
#include <string>
#include <string_view>
#include <vector>

#include <dune/logging/fmt.hh>
#include <dune/logging/logger.hh>
#include <dune/logging/logging.hh>

namespace Dune::Logging {

  namespace SinkMessageItems {

    inline auto localTime(const LogMessage& msg)
    {
      return LazyFormatArgument([&]()
        {
          return msg.localTime();
        });
    }

    inline auto relativeFraction(const LogMessage& msg)
    {
      return LazyFormatArgument([&]()
        {
          auto t = std::chrono::duration_cast<std::chrono::milliseconds>(msg.relativeTime());
          return (t - std::chrono::floor<std::chrono::seconds>(t)).count();
        });
    }

    inline auto relativeDays(const LogMessage& msg)
    {
      return LazyFormatArgument([&]()
        {
          return msg.relativeDays();
        });
    }

    inline auto backend(const LogMessage& msg, std::size_t width)
    {
      // This creates a stack buffer of size 30 for the backend name, which should avoid
      // a dynamic memory allocation in most cases
      return LazyFormatArgument([&,width,buffer=fmt::basic_memory_buffer<char, 30>()]() mutable -> std::string_view
        {
          auto logger = msg.logger().name();
          auto size = logger.size();
          if (size < width)
          {
            buffer.clear();
            buffer.append(logger.data(),logger.data() + size);
            buffer.resize(width);
            std::fill_n(buffer.data() + size,width - size,' ');
            return {buffer.data(),buffer.size()};
          }
          else
          {
            return logger;
          }
        });
    }

    inline auto mpiRank(const LogMessage& msg)
    {
      return LazyFormatArgument([&]()
        {
          return Logging::Logging::comm().rank();
        });
    }

  } // end namespace SinkMessageItems

} // end namespace Dune::Logging


#endif // DUNE_LOGGING_SINKMESSAGEITEMS_HH
