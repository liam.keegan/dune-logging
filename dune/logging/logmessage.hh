// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef DUNE_LOGGING_LOGMESSAGE_HH
#define DUNE_LOGGING_LOGMESSAGE_HH

#include <chrono>
#include <ctime>
#include <optional>
#include <string_view>

#include <dune/logging/fmt.hh>
#include <dune/logging/loglevel.hh>

namespace Dune::Logging {

  /**
   * \addtogroup logging
   * \{
   */

  class Logger;

  //! A log message as created by a Logger and passed on to its connected `Sink`s.
  class LogMessage
  {

    friend class LoggerBackend;

  public:

    //! The std::chrono clock used by the logging system.
    using Clock    = std::chrono::system_clock;

    //! The type used to represent the time point at which the message was logged.
    using Time     = Clock::time_point;

    //! The type used to represent the duration since program start.
    using Duration = Clock::duration;

    //! LogMessage cannot be copied.
    LogMessage(const LogMessage&) = delete;
    //! LogMessage cannot be copied.
    LogMessage& operator=(const LogMessage&) = delete;

  private:

    // LogMessage can only be constructed by LoggerBackend
    LogMessage(
      const Logger& logger,
      LogLevel level,
      int indent,
      std::string_view payload,
      Time time,
      Duration relative_time
      )
      : _payload(payload)
      , _level(level)
      , _indent(indent)
      , _logger(logger)
      , _time(time)
      , _relative_time(relative_time)
    {}

  public:

    //! Returns the formatted message logged by the user.
    std::string_view payload() const
    {
      return _payload;
    }

    //! Returns the log level specified by the user.
    LogLevel level() const
    {
      return _level;
    }

    //! Returns the requested indentation of the message.
    int indent() const
    {
      return _indent;
    }

    //! Returns a reference to the Logger that was used to log this message.
    const Logger& logger() const
    {
      return _logger;
    }

    //! Returns the absolute point in time at which this message was logged.
    Time time() const
    {
      return _time;
    }

    /**
     * \brief Returns the duration since program start (more precisely, since setup of the logging
     * system) at which this message was logged.
     */
    Duration relativeTime() const
    {
      return _relative_time;
    }

    //! Returns the sub-second part of time().
    std::chrono::nanoseconds absoluteSecondFraction() const
    {
      if (_absolute_second_fraction == Duration::zero())
      {
        auto seconds = std::chrono::floor<std::chrono::seconds>(_time);
        _absolute_second_fraction = _time - seconds;
      }
      return _absolute_second_fraction;
    }

    //! Returns the sub-second part of relativeTime().
    std::chrono::nanoseconds relativeSecondFraction() const
    {
      if (_relative_second_fraction == Duration::zero())
      {
        auto seconds = std::chrono::floor<std::chrono::seconds>(_relative_time);
        _relative_second_fraction = _relative_time - seconds;
      }
      return _relative_second_fraction;
    }

    //! Returns the total number of days since the program was started.
    int relativeDays() const
    {
      return std::chrono::duration_cast<std::chrono::duration<int,std::ratio<24*60*60>>>(_relative_time).count();
    }

    //! Returns local time of the point in time  at which this message was logged.
    /**
     * \warning The first call to this method is rather expensive, avoid it if you don't need to
     *          know the absolute point in time at which log messages occured. It might be a better
     *          alternative to record the relative time since program start instead. The return
     *          value is cached internally, so subsequent calls to this method on a given LogMessage
     *          are cheap.
     */
    const std::tm& localTime() const;

  private:

    std::string_view _payload;
    LogLevel _level;
    int _indent;
    const Logger& _logger;

    Time _time;
    Duration _relative_time;

    mutable std::chrono::nanoseconds _absolute_second_fraction = Duration::zero();
    mutable std::chrono::nanoseconds _relative_second_fraction = Duration::zero();

    mutable std::optional<std::tm> _local_time;

  };

  /**
   * \} logging
   */

} // end namespace Dune::Logging

#endif // DUNE_LOGGING_LOGMESSAGE_HH
