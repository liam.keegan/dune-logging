// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef DUNE_LOGGING_LOGGINGSTREAM_HH
#define DUNE_LOGGING_LOGGINGSTREAM_HH

#include <ostream>
#include <string>
#include <string_view>

#include <dune/logging/logger.hh>
#include <dune/logging/loggingstreambuffer.hh>

namespace Dune::Logging {

  /**
   * \addtogroup logging
   * \{
   */

  //! A std::ostream that forwards to a Logger.
  /**
   * This `std::ostream` does not directly output data, but forwards any input to its associated
   * logger. It can operate in two different modes:
   *
   * - In line-buffered mode, the buffer will only output complete lines that have been terminated
   *   by a newline character. In particular, this mode will ignore any explicit flushing of the C++
   *   stream. This mode is capable of exactly reproducing the original output layout as designed by
   *   the user of the `std::ostream`, but messages may appear later than expected when the user
   *   explicitly flushes the C++ stream.
   *
   * - In unbuffered mode, the buffer will always forward all pending data everytime the C++ stream
   *   is flushed. As most logging sinks are line-oriented and insert an additional newline after
   *   each log message, this will not correctly reproduce the original layout of the output. As a
   *   lot of people use `std::endl` instead of just `"\n"` for ending their lines, this mode will
   *   not forward empty lines to the logging system to avoid empty lines after every regular line
   *   printed to the C++ stream.
   *
   * \sa LoggingStreamBuffer
   */
  class LoggingStream
    : public std::ostream
  {

  public:

    //! Constructs a LoggingStream.
    LoggingStream(bool line_buffered, Logger stream_logger);

    //! Sets the logger used by this buffer.
    void setLogger(Logger logger);

    //! Returns a copy of the
    Logger logger() const;

  private:

    LoggingStreamBuffer _stream_buf;

  };

  /**
   * \}
   */

} // namespace Dune::Logging

#endif // DUNE_LOGGING_LOGGINGSTREAM_HH
