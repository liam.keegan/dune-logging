# We require CMake 3.9 for now
cmake_minimum_required(VERSION 3.9)

# Enable all CMake policies up to CMake 3.14
if (${CMAKE_VERSION} VERSION_LESS 3.14)
  cmake_policy(VERSION ${CMAKE_MAJOR_VERSION}.${CMAKE_MINOR_VERSION})
else()
  cmake_policy(VERSION 3.14)
endif()

project(dune-logging CXX)

if(NOT (dune-common_DIR OR dune-common_ROOT OR
      "${CMAKE_PREFIX_PATH}" MATCHES ".*dune-common.*"))
    string(REPLACE  ${CMAKE_PROJECT_NAME} dune-common dune-common_DIR
      ${PROJECT_BINARY_DIR})
endif()

#find dune-common and set the module path
find_package(dune-common REQUIRED)
list(APPEND CMAKE_MODULE_PATH "${PROJECT_SOURCE_DIR}/cmake/modules"
  ${dune-common_MODULE_PATH})

#include the dune macros
include(DuneMacros)

# start a dune project with information from dune.module
dune_project()

# Create dune-logging-fmt library if fmt was not found. That library will contain
# our vendored version of fmt
if(NOT TARGET fmt::fmt)
  set(fmt_lib dune-logging-fmt)
endif()

dune_enable_all_packages(
  MODULE_LIBRARIES
    ${fmt_lib}
    dune-logging
  )

# handle libfmt

if(TARGET fmt::fmt)

  # We are using an upstream version of libfmt, so link against that
  target_link_libraries(dune-logging PUBLIC fmt::fmt)

else()

  message(STATUS "Could not find installed version of fmtlib library, vendoring included version")

  # Make sure the user actually checked out the submodule with the vendored library
  if(NOT EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/vendor/fmt/src/format.cc)
    message(
      FATAL_ERROR "\
Could not find vendored fmtlib library, did you check out the submodule? Either delete the \
dune-logging source directory and check it out again with \"git clone --recursive\", or run \
the command \"git submodule update --init --recursive\" in the dune-logging source directory!"
      )
  endif()

  # Compile the fmtlib library sources into the dune-logging-fmt library
  dune_library_add_sources(
    dune-logging-fmt
    SOURCES
      vendor/fmt/src/format.cc
      vendor/fmt/src/posix.cc
    )

  # Find fmt headers
  file(
    GLOB FMT_HEADERS
    vendor/fmt/include/fmt/*.h
    )

  # Install vendored headers
  install(
    FILES ${FMT_HEADERS}
    DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/dune/vendor/fmt/fmt
    )

  if(${CMAKE_SYSTEM_NAME} STREQUAL "Windows")

    # In Windows, we can't just symlink the header directory of the vendored fmt library
    # to the build tree, so we actually have to copy the files over manually, which the
    # following block of code does. We don't just copy the headers at CMake generation time,
    # as that will not update the files if they have been changed.

    # Re-glob headers with relative path
    file(
      GLOB FMT_HEADERS
      RELATIVE "${CMAKE_CURRENT_SOURCE_DIR}/vendor/fmt/include/fmt"
      CONFIGURE_DEPENDS
      vendor/fmt/include/fmt/*.h
      )

    set(_headers "")

    foreach(_header ${FMT_HEADERS})

      add_custom_command(
        OUTPUT "${CMAKE_CURRENT_BINARY_DIR}/dune/vendor/fmt/fmt/${_header}"
        COMMAND "${CMAKE_COMMAND}"
        ARGS
          -E copy
          "${CMAKE_CURRENT_SOURCE_DIR}/vendor/fmt/include/fmt/${_header}"
          "${CMAKE_CURRENT_BINARY_DIR}/dune/vendor/fmt/fmt/${_header}"
        DEPENDS "${CMAKE_CURRENT_SOURCE_DIR}/vendor/fmt/include/fmt/${_header}"
        COMMENT "Copying fmt header ${_header}"
        )

      list(APPEND _headers "${CMAKE_CURRENT_BINARY_DIR}/dune/vendor/fmt/fmt/${_header}")

    endforeach()

    add_custom_target(fmt_headers DEPENDS ${_headers} COMMENT "Updating fmt headers")
    add_dependencies(dune-logging-fmt fmt_headers)

  else()

    # We are on a sane system with a normal filesystem, so symlink header directory in source tree to
    # vendor include directory in build tree
    message(STATUS "Symlinking fmt header directory to build tree")
    file(MAKE_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}/dune/vendor/fmt")
    execute_process(
      COMMAND
        "${CMAKE_COMMAND}" -E create_symlink
        "${CMAKE_CURRENT_SOURCE_DIR}/vendor/fmt/include/fmt"
        "${CMAKE_CURRENT_BINARY_DIR}/dune/vendor/fmt/fmt"
      )

  endif()

  # We need to set some preprocessor definions
  target_compile_definitions(
    dune-logging-fmt
    PRIVATE FMT_EXPORT
    INTERFACE $<$<NOT:$<STREQUAL:$<TARGET_PROPERTY:TYPE>,STATIC_LIBRARY>>:FMT_SHARED>
    PUBLIC DUNE_LOGGING_VENDORED_FMT=1
    )

  # Tell the compiler about the custom path of the fmt headers
  target_include_directories(
    dune-logging-fmt
    PUBLIC
      $<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}/dune/vendor/fmt>
      $<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}/dune/vendor/fmt>
    )

endif()

if (DUNE_HAVE_CXX_FLAG_DISABLE_STRING_LITERAL_OPERATOR_WARNING)
  target_compile_options(dune-logging PUBLIC -Wno-gnu-string-literal-operator-template)
endif()

add_subdirectory(cmake/modules)
add_subdirectory(dune)
add_subdirectory(doc/doxygen)


# finalize the dune project, e.g. generating config.h etc.
finalize_dune_project(GENERATE_CONFIG_H_CMAKE)
