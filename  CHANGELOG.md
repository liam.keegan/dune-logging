# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).
We follow the [DUNE release cycle](https://www.dune-project.org/releases/).

<!--
Guiding Principles

    Changelogs are for humans, not machines.
    There should be an entry for every single version.
    The same types of changes should be grouped.
    Versions and sections should be linkable.
    The latest version comes first.
    The release date of each version is displayed.
    Mention whether you follow Semantic Versioning.

Types of changes

    Added         for new features.
    Changed       for changes in existing functionality.
    Deprecated    for soon-to-be removed features.
    Removed       for now removed features.
    Fixed         for any bug fixes.
    Security      in case of vulnerabilities.
 -->

## [Unreleased] ([git-diff][Unreleased-diff])

## [2.8.0] ([git-diff][2.8.0-diff]) - 2021-07-29
### Added
 - Changelog !15
 - Support for `fmt` v8 !13
### Fixed
 - Expand `%F` and `%T` to have better support on Windows !15

## [2.7.1] - 2021-02-10
### Fixed
 - Fix fmt-installation-path !12

## [2.7.0] - 2021-02-10
### Added
 - First release


[Unreleased-diff]: https://gitlab.dune-project.org/staging/dune-logging/compare/v2.7.1...master
[[2.8.0-diff]: https://gitlab.dune-project.org/staging/dune-logging/compare/v2.7.1...releases/2.8

[Unreleased]: https://gitlab.dune-project.org/staging/dune-logging/-/tree/master
[2.7.1]: https://gitlab.dune-project.org/staging/dune-logging/-/v2.7.1
[2.7.0]: https://gitlab.dune-project.org/staging/dune-logging/-/v2.7.0
